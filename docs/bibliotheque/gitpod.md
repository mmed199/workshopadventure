# GITPOD

[Gitpod](https://gitpod.iod) est un environnement de développement "dans les nuages". Il permet de bénéficier d'un environnement complet au travers d'un navigateur (et pas que).

Nous vous proposerons de l'utiliser principalement pour lancer l'installation de votre cluster kubernetes puis ensuite pour interagir avec.

Dans le cadre de cet atelier, vous l'utiliserez donc principalement :

- le shell de gitpod: pour lancer des scripts et diverses lignes de commandes
- pour éditer des fichiers

Mais sacher que vous pouvez aussi vous en servir en mode visuel pour la partie git.

GitPod au travers de VSCode ou IntelliJ peut aussi être utilisé comme plateforme de développement pour de nombreux langages, mais c'est une autre aventure qui se déroule dans le royaume voisin de la compilation, ce qui est une autre histoire (périeuse car les trolls règnent dans ce royaume, particulièrement à la frontières des contrèes Java et JavaScript).

Vous pouvez directement lancer GitPod depuis un projet GitLab en choisissant depuis la page principale du projet dans la combo-box situé en haut `Gitpod` à la place de `Web IDE` puis en cliquant ensuite sur `Gitpod`, il faut ensuite choisir de vous identifier en utilisant votre compte Gitlab.

Le premier lancement est un peu plus long, une fois le projet lancé celui-ci sera accessible (pour le relancer, le supprimer, ...) via le [dashboard](https://gitpod.io/workspaces). Pour ce projet, vous utiliserez un livre de sort Gitpod modifié pour contenir tous les sortilèges et potions vous permettant d'interagir avec kubernetes et une bonne partie des créatures qui peuple cet univers.

En usage gratuit, vous avez droit à 50h par mois.

## Commandes utiles

Les commandes de base :

- copier
  - dans le shell : `<CTRL> + <SHIFT> + <C>`
  - dans l'éditeur : `<CTRL> + <C>`
- coller : `<CTRL> + <SHIFT> + <V>`
  - dans le shell : `<CTRL> + <SHIFT> + <V>`
  - dans l'éditeur : `<CTRL> + <V>`
- ouvrir un nouveau shell depuis l'éditeur : `<CTRL> + <SHIFT> + <C>`
- fermer le workspace : naviguer dans le menu en haut à gauche (3 barres horizontales) puis "Gitpod : Stop Workspace"

Si vous connaissez vscode vous ne serez pas dépaysé.

Pour en savoir plus sur Gitpod, suivez [les guides](https://www.gitpod.io/guides) (mais plus tard, là vous n'aurez pas le temps)

Dans le menu choisir "Gitpod : Open Context" ramène sur le projet gitlab
