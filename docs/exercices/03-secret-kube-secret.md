# Fonctionnement des secrets kubernetes

Il existe une ressource `secret` dans kubernetes, celle ci permet de stocker des informations avec une petite protection appliquée :

- controle d'accès aux secret (RBAC)
- capacité de chiffrer dans ETCD ces objets si l'on met en place le mécanisme dit de [encryption of secret data at rest](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/)

Néanmoins il faut savoir que les manifest des objets de type `secret` ne sont pas sécurisé. Le secret est juste encodé en base 64 😳.

Pour créer un manifest d'un secret il suffit d'utiliser la commande impérative de création d'un secret combiné à avec `--dry-run ....` ou de créer le fichier à la main et d'encoder en base64 la valeur du secret.

<secret-name> <namespace> <clef> <value>
Exemple:

```shell
kubectl create secret generic nom_du_secret -n namespace_du_secret --dry-run=client --from-literal=password="Mot de passe très secret" -o yaml > secret.yaml
```

Cela va créer le fichier "secret.yaml" dont le contenu sera

```yaml
apiVersion: v1
data:
  password: TW90IGRlIHBhc3NlIHRyw6hzIHNlY3JldA==
kind: Secret
metadata:
  creationTimestamp: null
  name: nom_du_secret
  namespace: namespace_du_secret
```

🙈 Remarquez que l'on décode très facilement le pseudo "secret"

```shell
$ echo "TW90IGRlIHBhc3NlIHRyw6hzIHNlY3JldA==" | base64 -d
Mot de passe très secret
```
